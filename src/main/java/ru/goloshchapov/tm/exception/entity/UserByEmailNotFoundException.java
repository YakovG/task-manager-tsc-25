package ru.goloshchapov.tm.exception.entity;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.AbstractException;

public class UserByEmailNotFoundException extends AbstractException {

    public UserByEmailNotFoundException(@Nullable final String message) {
        super("Error! User with EMAIL: " + message + " not found");
    }

}
