package ru.goloshchapov.tm.exception.entity;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.AbstractException;

public class UserByIdNotFoundException extends AbstractException {

    public UserByIdNotFoundException(@Nullable final String message) {
        super("Error! User with ID: " + message + " not found...");
    }

}
