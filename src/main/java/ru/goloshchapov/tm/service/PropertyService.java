package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String VERSION = "version";

    @NotNull
    private static final String VERSION_DEFAULT = "";

    @NotNull
    private static final String AUTHOR = "author";

    @NotNull
    private static final String AUTHOR_DEFAULT = "";

    @NotNull
    private static final String EMAIL = "email";

    @NotNull
    private static final String EMAIL_DEFAULT = "";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY))
            return System.getenv(PASSWORD_SECRET_KEY);
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY))
            return System.getProperty(PASSWORD_SECRET_KEY);
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            final String value = System.getenv(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().containsKey(PASSWORD_ITERATION_KEY)) {
            final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        final String value = properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getenv().containsKey(VERSION))
            return System.getenv(VERSION);
        if (System.getProperties().containsKey(VERSION))
            return System.getProperty(VERSION);
        return properties.getProperty(VERSION, VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationAuthor() {
        if (System.getenv().containsKey(AUTHOR))
            return System.getenv(AUTHOR);
        if (System.getProperties().containsKey(AUTHOR))
            return System.getProperty(AUTHOR);
        return properties.getProperty(AUTHOR, AUTHOR_DEFAULT);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        if (System.getenv().containsKey(EMAIL))
            return System.getenv(EMAIL);
        if (System.getProperties().containsKey(EMAIL))
            return System.getProperty(EMAIL);
        return properties.getProperty(EMAIL, EMAIL_DEFAULT);
    }

}
