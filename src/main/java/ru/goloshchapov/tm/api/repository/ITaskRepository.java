package ru.goloshchapov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @Nullable List<Task> findAllByProjectId(String userId, String projectId);

    @Nullable Task bindToProjectById (String userId, String taskId, String projectId);

    @Nullable Task unbindFromProjectById(String userId, String taskId);

    @Nullable List<Task> removeAllByProjectId(String userId, String projectId);

}
