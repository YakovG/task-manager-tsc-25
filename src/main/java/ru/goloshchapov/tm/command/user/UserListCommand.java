package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class UserListCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-list";

    @NotNull public static final String DESCRIPTION = "Show user list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST]");
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        for (@NotNull final User user:users) {
            if (user.isLocked()) System.out.print("LOCKED! ");
            System.out.print(user.getLogin()+ " ");
            if (!isEmpty(user.getEmail())) System.out.print(user.getEmail() + " ");
            System.out.println(user.getRole().getDisplayName());
        }
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
