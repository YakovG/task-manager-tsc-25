package ru.goloshchapov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;

public final class UsersByDefaultCreateCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-create-by-default";

    @NotNull public static final String DESCRIPTION = "Create default users";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getUserService().create("admin","admin", Role.ADMIN).setId("aaa");
        serviceLocator.getUserService().create("test","test","test@test.tt").setId("ttt");
        serviceLocator.getUserService().create("demo","demo", "demo@demo.dd").setId("ddd");
    }

}
